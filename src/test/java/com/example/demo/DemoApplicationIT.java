package com.example.demo;

import com.example.demo.controller.ToDoController;

import com.example.demo.model.ToDoEntity;
import com.example.demo.service.ToDoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
class DemoApplicationIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ToDoController toDoController;

    @Autowired
    private ToDoService toDoService;

    @Test
    void contextLoads() throws Exception {
        if (toDoController == null) {
            throw new Exception("ToDoController is null");
        }
    }
    
    // #MY_TEST
    @Test
    void whenComplete_thenReturnValidResponse() throws Exception {
        Long testId = 1L;
        this.mockMvc
                .perform(put("/todos/" + testId + "/complete"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.text").isString())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.completedAt").exists());
    }

}

package com.example.demo.controller;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.service.ToDoService;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
@Import(ToDoService.class)
class ToDoControllerWithServiceIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ToDoRepository toDoRepository;

    // #MY_TEST
    @Test
    void whenGetAllFiltered_thenReturnValidResponse() throws Exception {
        String testText = "Do homework";
        Long testId = 1L;
        String testQuery = "homework";

        List<ToDoEntity> testToDos = new ArrayList<>();
        testToDos.add(new ToDoEntity(testId, testText));
        testToDos.add(new ToDoEntity(2L, "Do sport"));


        when(toDoRepository.findAllByTextContaining(Mockito.anyString())).thenReturn(
                testToDos.stream()
                        .filter(t -> t.getText().contains(testQuery))
                        .collect(Collectors.toList())
        );

        this.mockMvc
                .perform(get("/todos/filtered")
                        .param("query", testQuery))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].text").value(testText))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(testId))
                .andExpect(jsonPath("$[0].completedAt").doesNotExist());
    }

    // #MY_TEST
    @Test
    void whenDelete_thenReturnValidResponse() throws Exception {
        Long testId = 1L;

        doAnswer((i) -> null).when(toDoRepository).deleteById(testId);

        this.mockMvc
                .perform(delete("/todos/" + testId))
                .andExpect(status().is2xxSuccessful());
    }

    // #MY_TEST_WITH_EXCEPTION
    @Test
    void whenGetAllByWrongIds_thenThrowNotFoundException() throws Exception {

        List<ToDoEntity> testToDos = new ArrayList<>();
        List<Long> queryIds = new ArrayList<>();
        queryIds.add(1L);
        queryIds.add(2L);
        queryIds.add(5L);

        testToDos.add(new ToDoEntity(1L, "Do homework"));
        testToDos.add(new ToDoEntity(2L, "Ride bike"));
        testToDos.add(new ToDoEntity(3L, "Play tennis"));
        testToDos.add(new ToDoEntity(4L, "Do sport"));


        List<ToDoEntity> filteredTestToDos = testToDos.stream()
                .filter(f -> queryIds.contains(f.getId()))
                .collect(Collectors.toList());

        when(toDoRepository.findAllByIds(Mockito.anyList()))
                .thenReturn(filteredTestToDos);


        this.mockMvc
                .perform(get("/todos/all")
                        .param("ids", queryIds.stream().map(String::valueOf).collect(Collectors.joining(","))))
                .andExpect(status().isNotFound());

    }

    @Test
    void whenGetAll_thenReturnValidResponse() throws Exception {
        String testText = "My to do text";
        when(toDoRepository.findAll()).thenReturn(
                Arrays.asList(
                        new ToDoEntity(1l, testText)
                )
        );

        this.mockMvc
                .perform(get("/todos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].text").value(testText))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].completedAt").doesNotExist());
    }

}

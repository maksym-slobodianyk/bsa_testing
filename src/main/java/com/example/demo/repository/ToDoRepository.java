package com.example.demo.repository;

import com.example.demo.model.ToDoEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ToDoRepository extends JpaRepository<ToDoEntity, Long> {

    List<ToDoEntity> findAllByTextContaining(String text);

    List<ToDoEntity> findAll();

    @Query("SELECT t FROM ToDoEntity t where t.id IN :ids")
    List<ToDoEntity> findAllByIds(List<Long> ids);

}
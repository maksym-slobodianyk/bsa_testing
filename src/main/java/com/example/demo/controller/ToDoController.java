package com.example.demo.controller;

import com.example.demo.exception.ToDoNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import javax.validation.Valid;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.service.ToDoService;

import org.springframework.beans.factory.annotation.Autowired;

@RestController
public class ToDoController {

	@Autowired
	ToDoService toDoService;

	@GetMapping("/todos")
	@Valid List<ToDoResponse> getAll() {
		return toDoService.getAll();
	}

	@GetMapping("/todos/filtered")
	@Valid List<ToDoResponse> getAllFiltered(@RequestParam(required = true) String query) {
		return toDoService.getAllFiltered(query);
	}

	@PostMapping("/todos")
	@Valid ToDoResponse save(@Valid @RequestBody ToDoSaveRequest todoSaveRequest) throws ToDoNotFoundException {
		return toDoService.upsert(todoSaveRequest);
	}

	@PutMapping("/todos/{id}/complete")
	@Valid ToDoResponse save(@PathVariable Long id) throws ToDoNotFoundException {
		return toDoService.completeToDo(id);
	}

	@GetMapping("/todos/{id}")
	@Valid ToDoResponse getOne(@PathVariable Long id) throws ToDoNotFoundException {
		return toDoService.getOne(id);
	}

	@GetMapping("/todos/all")
	@Valid List<ToDoResponse> getByIds(@RequestParam List<Long> ids) throws ToDoNotFoundException {
		return toDoService.getAllByIds(ids);
	}


	@DeleteMapping("/todos/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	void delete(@PathVariable Long id) {
		toDoService.deleteOne(id);
	}

}
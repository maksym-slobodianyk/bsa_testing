package com.example.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Can not find todo")
public class ToDoNotFoundException extends RuntimeException {

    public ToDoNotFoundException() {
    }

    public ToDoNotFoundException(Long id) {
        super(String.format("Can not find todo with id %d", id));
    }

}
